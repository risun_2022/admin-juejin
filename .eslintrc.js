module.exports = {
  /**
   * 默认情况下，ESLint会在所有父级目录里寻找配置文件，一直到根目录。
   * 为了将ESLint限制在一个特定的项目，设置root: true；
   * ESLint一旦发现配置文件中有 root: true，就会停止在父级目录中寻找。
   */
  root: true,
  // env: 指定脚本的运行环境
  env: {
    browser: true, // 浏览器环境中的全局变量。
    es2021: true,
    node: true, // Node.js 全局变量和 Node.js 作用域。
  },
  // 配置文件从基础配置中继承已启用的规则。
  extends: [
    'vue-global-api',
    'plugin:prettier/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:@typescript-eslint/recommended',
    '@vue/eslint-config-prettier',
  ],
  // 指定解析器
  // 设置解析器能帮助ESLint确定什么是解析错误。
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 2021,
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  // 使用第三方插件。全局安装的 ESLint 实例只能使用全局安装的ESLint插件。本地同理，不支持混用。
  plugins: ['vue', '@typescript-eslint'],
  /**
   * 每个规则有【3】个错误级别。
   * off或0: 关闭该规则；
   * warn或1: 开启规则，使用警告级别的错误，不会导致程序退出；
   * error或2: 开启规则，使用错误级别的错误，当被触发的时候，程序会退出。
   */
  rules: {
    'prettier/prettier': 'warn',
    'no-undef': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/comment-directive': 'off',
    'vue/no-v-html': 'off',
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'any',
          normal: 'any',
          component: 'always',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'vue/multi-word-component-names': 'off',
    'vue/multiline-html-element-content-newline': 'off', // 强制在多行元素的内容前后加换行符
    'vue/singleline-html-element-content-newline': 'off', // 强制在单行元素的内容前后加换行符
    // Vue.js风格指南 https://v3.cn.vuejs.org/style-guide/
    // Vue组件排序
    'vue/order-in-components': [
      'warn',
      {
        order: ['el', 'name', 'key', 'parent', 'functional'],
      },
    ],
    // Vue属性排序
    'vue/attributes-order': [
      'warn',
      {
        order: ['DEFINITION', 'LIST_RENDERING', 'CONDITIONALS'],
      },
    ],
    '@typescript-eslint/ban-ts-ignore': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/no-use-before-define': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
  },
}

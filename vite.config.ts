
import { UserConfig, ConfigEnv, loadEnv } from 'vite'
import { createVitePlugins, createViteServer, createViteBuild } from './config'
import { warpperEnv } from './config/utils'
import { resolve } from 'path'
// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfig => {
  const isBuild = command === 'build'
  const env = loadEnv(mode, process.cwd())
  const viteEnv = warpperEnv(env)
  const { VITE_PUBLIC_PATH } = viteEnv
  return {
    base: VITE_PUBLIC_PATH,
    plugins: createVitePlugins(viteEnv, isBuild),
    server: createViteServer(viteEnv),
    build: createViteBuild(isBuild),
    resolve: {
      // https://cn.vitejs.dev/config/#resolve-alias
      alias: {
        '~': resolve(__dirname, '.'),
        '@': resolve(__dirname, 'src'),
        '#': resolve(__dirname, 'types'),
        '@library': resolve(__dirname, 'library'),
      },
      // https://cn.vitejs.dev/config/#resolve-extensions
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    },
  }
}

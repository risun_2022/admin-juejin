import { getConfig } from '@/config'
import { useGlobal } from '@pureadmin/utils'
import { transformI18n } from '@/i18n'
import { routeMetaType } from '../types'
import { useEpThemeStoreHook } from '~/src/store/modules/theme'

export function useNav() {
  /** 设置国际化选中后的样式 */
  const getDropdownItemStyle = computed(() => {
    return (locale, t) => {
      return {
        background: locale === t ? useEpThemeStoreHook().epThemeColor : '',
        color: locale === t ? '#f4f4f5' : '#000',
      }
    }
  })

  const getDropdownItemClass = computed(() => {
    return (locale, t) => {
      return locale === t ? '' : 'dark:hover:!text-primary'
    }
  })

  const { $storage, $config } = useGlobal<GlobalPropertiesApi>()

  const title = computed(() => {
    return $config.Title
  })

  /** 动态title */
  function changeTitle(meta: routeMetaType) {
    const Title = getConfig().Title
    if (Title) document.title = `${transformI18n(meta.title)} | ${Title}`
    else document.title = transformI18n(meta.title)
  }

  function handleResize(menuRef) {
    menuRef?.handleResize()
  }
  return {
    title,
    $storage,
    changeTitle,
    handleResize,
    getDropdownItemStyle,
    getDropdownItemClass,
  }
}

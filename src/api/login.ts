import { http } from '@/utils/http'
type Result = {
  success: boolean
  data: Array<any>
}

export type UserResult = {
  code: number
  data: {
    /** `token` */
    access_token: string
    /** user_token */
    user_token: string
    /** `accessToken`的过期时间（格式'xxxx/xx/xx xx:xx:xx'） */
    expires_in: Date
  }
}

/** 登录 */
export const login = (data?: object) => {
  return http.request<UserResult>('post', '/basic-api/auth/login', { data })
}

export const getWechat = () => {
  return http.request<string>('get', '/basic-api/auth/WXLogin')
}

import 'vue-global-api'
import App from './App.vue'
import { createApp } from 'vue'
import { bootstrap } from '~/library'
import { setupI18n } from '@/i18n'
import { setupStore } from '@/store'
import { setupRouter } from '@/router'
import { getServerConfig } from '@/config'
import { injectResponsiveStorage } from '@/utils/responsive'
// 全局注册`@iconify/vue`图标库
import { IconifyIconOffline, IconifyIconOnline, FontIcon } from '@/components/ReIcon'
// 引入重置样式
import './styles/reset.scss'
// 加载全局样式样式
import './styles/index.scss'
// 一定要在main.ts中导入tailwind.css，防止vite每次hmr都会请求src/style/index.scss整体css文件导致热更新慢的问题
import './styles/tailwind.css'
const app = createApp(App)
app.component('IconifyIconOffline', IconifyIconOffline)
app.component('IconifyIconOnline', IconifyIconOnline)
app.component('FontIcon', FontIcon)
getServerConfig(app).then(async (config) => {
  injectResponsiveStorage(app, config)
  bootstrap(app)
  setupI18n(app)
  setupStore(app)
  await setupRouter(app)
    .isReady()
    .then(() => app.mount('#app'))
})

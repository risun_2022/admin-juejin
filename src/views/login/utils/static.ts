import bg from '@/assets/login/bg.png'
import users from '@/assets/login/login_users.png'
import Check from '@iconify-icons/ep/check'
import Lock from '@iconify-icons/ri/lock-fill'
import User from '@iconify-icons/ri/user-3-fill'
export { bg, users, Check, Lock, User }

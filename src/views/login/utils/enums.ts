import { $t } from '@/i18n'

const operates = [
  {
    title: $t('login.phoneLogin'),
  },
  {
    title: $t('login.weChatLogin'),
  },
]

export { operates }

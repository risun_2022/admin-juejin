import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import vueSetupExtend from 'vite-plugin-vue-setup-extend'
import VueI18n from '@intlify/unplugin-vue-i18n/vite'
import vueJsx from '@vitejs/plugin-vue-jsx'
import svgLoader from 'vite-svg-loader'
import { visualizer } from 'rollup-plugin-visualizer'
import { configBuildInfo } from './info'
import { configMockPlugin } from './mock'
import { configCompressPlugin } from './compress'
import { configImageminPlugin } from './imagemin'
import { configStyleImportPlugin } from './style'
import { configAutoImportPlugin, configVueComponentsPlugin, configVueIconsPlugin } from './unplugin'
export function createVitePlugins(viteEnv: ViteEnv, isBuild: boolean) {
  const {
    VITE_USE_IMAGEMIN,
    VITE_USE_COMPRESS,
    VITE_USE_MOCK,
    VITE_COMPRESSION,
    VITE_COMPRESS_DELETE_ORIGIN_FILE,
  } = viteEnv
  const lifecycle = process.env.npm_lifecycle_event
  const plugins = [
    vue(),
    vueSetupExtend(),
    // https://github.com/intlify/bundle-tools/tree/main/packages/vite-plugin-vue-i18n
    VueI18n({
      runtimeOnly: true,
      compositionOnly: true,
      include: [resolve('locales/**')],
    }),
    // jsx、tsx语法支持
    vueJsx(),
    // svg组件化支持
    svgLoader(),
    // 打包分析
    lifecycle === 'report'
      ? visualizer({ open: true, brotliSize: true, filename: 'report.html' })
      : null,
    // build-info
    configBuildInfo(),
    // unplugin-auto-import
    configAutoImportPlugin(),
    // unplugin-vue-components
    configVueComponentsPlugin(),
    // vite-plugin-style-import
    configStyleImportPlugin(),
    // unplugin-icons
    configVueIconsPlugin(),
  ]
  // vite-plugin-mock
  VITE_USE_MOCK && plugins.push(configMockPlugin(isBuild))
  if (isBuild) {
    // vite-plugin-compress
    VITE_USE_COMPRESS &&
      plugins.push(configCompressPlugin(VITE_COMPRESS_DELETE_ORIGIN_FILE, VITE_COMPRESSION))
    // vite-plugin-imagemin
    VITE_USE_IMAGEMIN && plugins.push(configImageminPlugin())
  }
  return plugins
}

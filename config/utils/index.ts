// 处理环境变量
const warpperEnv = (envConf: Recordable): ViteEnv => {
  /** 此处为默认值 */
  const ret: ViteEnv = {
    VITE_APP_TITLE: '',
    VITE_PUBLIC_PATH: '',
    VITE_ROUTER_HISTORY: '',
    VITE_PORT: 8848,
    VITE_PROXY: [
      ['/basic-api', 'http://localhost:3000'],
      ['/upload', 'http://localhost:3300/upload'],
    ],
    VITE_DROP_CONSOLE: false,
    VITE_USE_IMAGEMIN: false,
    VITE_USE_COMPRESS: false,
    VITE_USE_MOCK: false,
    VITE_COMPRESS_DELETE_ORIGIN_FILE: false,
    VITE_COMPRESSION: 'none',
  }
  for (const envName of Object.keys(envConf)) {
    let realName = envConf[envName].replace(/\\n/g, '\n')
    realName = realName === 'true' ? true : realName === 'false' ? false : realName

    if (envName === 'VITE_PORT') {
      realName = Number(realName)
    }
    if (envName === 'VITE_PROXY' && realName) {
      try {
        realName = JSON.parse(realName.replace(/'/g, '"'))
      } catch (error) {
        realName = ''
      }
    }
    ret[envName] = realName
    if (typeof realName === 'string') {
      process.env[envName] = realName
    } else if (typeof realName === 'object') {
      process.env[envName] = JSON.stringify(realName)
    }
  }
  return ret
}
// 跨域代理重写
const regExps = (value: string, reg: string): string => {
  return value.replace(new RegExp(`^${reg}`, 'g'), '')
}

export { warpperEnv, regExps }

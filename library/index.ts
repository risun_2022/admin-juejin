import { App } from 'vue'
export function bootstrap(app: App<Element>) {
  // 加载插件
  const Plugins: any = import.meta.glob('./plugins/*.ts', { eager: true })
  Object.keys(Plugins).forEach((key) => {
    const plugin = Plugins[key]
    plugin.setup(app)
  })
}
